package com.biom4st3r.incense.mxn;

import com.biom4st3r.incense.interfaces.Incense;

import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import net.minecraft.block.BlockState;
import net.minecraft.block.CampfireBlock;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.CampfireBlockEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionUtil;
import net.minecraft.potion.Potions;
import net.minecraft.recipe.BrewingRecipeRegistry;
import net.minecraft.state.property.BooleanProperty;
import net.minecraft.util.Hand;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

@Mixin(CampfireBlock.class)
public abstract class CampFireBlockMxn 
{
    @Final
    @Shadow
    public static BooleanProperty LIT;

    @Inject(at = @At("RETURN"),method = "activate",cancellable = true)
    public void activate(BlockState blockState, World world, BlockPos blockPos, 
        PlayerEntity pe, Hand hand, BlockHitResult blockHitResult, 
        CallbackInfoReturnable<Boolean> ci)
    {
        if(!ci.getReturnValue())
        {
            if(blockState.get(LIT))
            {
                BlockEntity be = world.getBlockEntity(blockPos);
                if(be instanceof CampfireBlockEntity)
                {
                    CampfireBlockEntity campFire = (CampfireBlockEntity)be;
                    ItemStack iS = pe.getStackInHand(hand);
                    ItemStack awkardPotion = new ItemStack(Items.POTION,1);
                    awkardPotion = PotionUtil.setPotion(awkardPotion, Potions.AWKWARD);
                    //System.out.println(String.format("%s + %s = ",awkardPotion.getName().asFormattedString(),iS.getName().asFormattedString()));
                    if(BrewingRecipeRegistry.hasRecipe(awkardPotion, iS))
                    {
                        //System.out.println(BrewingRecipeRegistry.craft(iS,awkardPotion).getName().asFormattedString());
                        ItemStack potionStack = BrewingRecipeRegistry.craft(iS,awkardPotion);
                        Potion result = PotionUtil.getPotion(potionStack);
                        // System.out.println(potionStack.getName().asFormattedString());
                        // System.out.println(result.getEffects().get(0).getTranslationKey());
                        // System.out.println(((Incense)campFire).addPotion(result));
                        // System.out.println("added Potions");
                        if(((Incense)campFire).addPotion(result))
                        {
                            pe.getStackInHand(hand).decrement(1);
                        }
                        ci.setReturnValue(true);
                        ci.cancel();
                    }

                }
            }
        }

    }

}